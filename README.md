# ZLW Similarity

This program draws a graph where the nodes are binary data and the edges between these nodes are based on the variance of
the branch lengths of the Ziv-Lempel-Welch trees built from the data.

## Intro

At the University of Debrecen, we have been working with a Ziv-Lempel-Welch [1] tree building sample program for years in our
introductory C++ course called “*High level programming languages*”. We investigate the branch lengths of the ZLW tree
(as it is emphasized in [2], namely if variance (to be more precise the standard deviation) of the length of the branches is large,
it indicates a strong internal correlation of data).

In this semester, we have compared some COVID-19 genomes. Now, we wanted to draw „filogenetic trees” like
[CompLearn](https://github.com/rudi-cilibrasi/libcomplearn) package can do from these data.
The [man page of the ncd command](http://manpages.ubuntu.com/manpages/precise/man1/ncd.1.html)
of Rudi Cilibrasi’s CompLearn package gives
the idea to build an ZLW tree from two genomes. We can easily “concatenate” the two genomes during
the building the ZLW tree letter to letter.
Simply, one letter is taken from one genome (see *isbi1* in the below code snippet) and the other from the other genome (*isbi2*),
something like this:

```c++
ZLWTree<char, '/', '0'> zlwBt;
...
               for ( std::istreambuf_iterator<char> isbi1 {ifs1}, isbi2 {ifs2};
                         isbi1 != isbi1End || isbi2 != isbi2End;
                   ) {
                    if ( isbi1 != isbi1End ) {
                         zlwBt << *isbi1;
                         ++isbi1;
                    }
                    if ( isbi2 != isbi2End ) {
                         zlwBt << *isbi2;
                         ++isbi2;
                    }
```

## Usage

For example, let's investigate the mentioned COVID-19 genomes.
Data must be downloaded and saved to folder called **genomes**. COVID-19 data are available at
[https://www.ncbi.nlm.nih.gov/genbank/sars-cov-2-seqs/](https://www.ncbi.nlm.nih.gov/genbank/sars-cov-2-seqs/).
We have used FASTA data. Ignoring the first line of comment, these files are
typically consisting of the letters of the alphabet T, C, A, G.
So we must convert them into binary sequences (because our tree is binary), for example,
it can be done easily using the following code snippet

```c++
#include <iostream>
#include <iterator>
#include <bitset>
#include <algorithm>

int main()
{
    std::istream_iterator<char> isiEnd, isi{std::cin};
    std::ostream_iterator<std::bitset<8>> osi{std::cout};
    std::copy(isi, isiEnd, osi);

    return 0;
}
```
(where we take advantage that istream_iterator ignores the newline chars.)
After this the following files have already been binary sequences.

```bash
batfai@bhaxor:~/Dokumentumok/BHS/z3a7-z3a19/ZLWSimilarity/COVID19$ ls -l genomes

-rw-r--r-- 1 batfai batfai 151672 ápr    7 19:09 AF0868332EbolaMayingaZaire1976
-rw-r--r-- 1 batfai batfai 150968 ápr    7 19:13 KY7860271Ebola2001
-rw-r--r-- 1 batfai batfai 238928 ápr    6 17:11 LR757998China:Wuhan2019-12-26
-rw-r--r-- 1 batfai batfai 239224 ápr    6 17:04 MN908947China2019-12
-rw-r--r-- 1 batfai batfai 238704 ápr    6 17:50 MN938384China:Shenzhen2020-01-10
-rw-r--r-- 1 batfai batfai 239056 ápr    6 18:04 MN985325USA:WA2020-01-19
-rw-r--r-- 1 batfai batfai 239056 ápr    6 18:09 MN988713USA:IL2020-01-21
-rw-r--r-- 1 batfai batfai 239064 ápr    6 18:40 MN994468USA:CA2020-01-22
-rw-r--r-- 1 batfai batfai 238600 ápr    6 17:26 MN996527China:Wuhan2019-12-30
-rw-r--r-- 1 batfai batfai 239192 ápr    6 17:08 MT019529China:Wuhan2019-12-23
-rw-r--r-- 1 batfai batfai 239056 ápr    6 18:43 MT027062USA:CA2020-01-29
-rw-r--r-- 1 batfai batfai 238664 ápr    6 18:15 MT039873China:Hangzhou2020-01-20
-rw-r--r-- 1 batfai batfai 239056 ápr    6 18:58 MT044257USA:IL2020-01-28
-rw-r--r-- 1 batfai batfai 239096 ápr    6 18:52 MT258383USA:CA2020-03-18
-rw-r--r-- 1 batfai batfai 238944 ápr    6 17:53 MT259226China:Hubei,Wuhan2020-01-10
-rw-r--r-- 1 batfai batfai 239336 ápr    6 18:54 MT263459USA:WA2020-03-24
-rw-r--r-- 1 batfai batfai 239056 ápr    6 19:01 MT276326USA:GA2020-02-29
```
such as

```bash
$ more genomes/AF0868332EbolaMayingaZaire1976
01000011010001110...
```
The sources must be compiled with GCC8 because they use C++17 feature (and Boost library is also necessary to build because of BGL)

```bash
$ g++-8 main.cpp -o main -std=c++17 -lstdc++fs -lboost_system
```

Running

```bash
$ ./main
```

The resulted graph is generated as a DOT file from that we can create the figure of the graph with Graphviz's dot:

```bash
$ dot -Tpng teszt.dot -o teszt.png
```
Result

```bash
$ eog teszt.png
```

![teszt.png](teszt.png "teszt.png")

The left side of this figure (at this moment) contains a hodge podge of COVID-19 genomes but in the upper right corner
it can be seen well that this method can distinguish the two Ebola and the 15 COVID-19 genomes (there are no edges between
COVID-19 and Ebola genomes).


Question: is this a known result? I am not aware of that. I think the investigation of the branch lengths of the ZLW tree
can be associated with the Normalized Compression Distance [3] or more generally with the Ziv-Lempel complexity
but I beleave this contact is not trivial.

Is it an interesting result or a result at all? By studying the size of the input genom samples it should be noticed that the two Ebola genomes are smaller than the COVID-19 genomes. If we [experiment](https://www.twitch.tv/videos/609053887) with trying to harmonize the size of the genomes, the program responds differently.

For example, the following modification cannot distinguish the two Ebola and the 15 COVID-19 genomes:

```c++
for ( std::istreambuf_iterator<char> isbi1 {ifs1}, isbi2 {ifs2};
          isbi1 != isbi1End || isbi2 != isbi2End;
    ) {


     if ( isbi1 != isbi1End && isbi2 != isbi2End) {
          zlwBt << *isbi1;
          ++isbi1;
          zlwBt << *isbi2;
          ++isbi2;
     }
     else if ( isbi1 != isbi1End ) {
          zlwBt << *isbi1;
          zlwBt << *isbi1;
          ++isbi1;
     }
     else if ( isbi2 != isbi2End ) {
          zlwBt << *isbi2;
          zlwBt << *isbi2;
          ++isbi2;
     }
```

![teszt2.png](teszt2.png "tesz2.png")


But this modification can distinguish again the two Ebola and the 15 COVID-19 genomes:

```c++
for ( std::istreambuf_iterator<char> isbi1 {ifs1}, isbi2 {ifs2};
          isbi1 != isbi1End || isbi2 != isbi2End;
    ) {

     if ( isbi1 != isbi1End && isbi2 != isbi2End) {
          zlwBt << *isbi1;
          ++isbi1;
          zlwBt << *isbi2;
          ++isbi2;
     }
     else  {
          isbi1 = isbi1End;
          isbi2 = isbi2End;
     }     
```
![teszt3.png](teszt3.png "teszt3.png")


## Acknowledgment

I would like to thank the students of the BSc course “High Level Programming Languages” at the University of Debrecen and the viewers of the channel [BHAX](https://www.twitch.tv/nbatfai) for their interest.

## Contact

Norbert Bátfai, PhD  
UD, IT Dept.,
[https://www.inf.unideb.hu/batfai.norbert/](https://www.inf.unideb.hu/batfai.norbert/)

## References

[1] Rónyai Lajos, Ivanyos Gábor és Szabó Réka (2008) Algoritmusok, Typotex.  
[2] Tusnády Gábor (1996) Sztochasztikus számítástechnika, KLTE, pp.45, https://users.renyi.hu/~tusnady/mind.pdf  
[3] Ming Li, Xin Chen, Xin Li, Bin Ma and P. M. B. Vitanyi (2004) The similarity metric,
IEEE Transactions on Information Theory, 50(12), pp. 3250-3264., https://homepages.cwi.nl/~paulv/papers/similarity.pdf

Last modified: Mon, 5 May 2020 14:08:00 GMT
