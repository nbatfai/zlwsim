/*

ZLW Similarity
The generator program, https://gitlab.com/nbatfai/zlwsim

This program draws a graph where the nodes are binary data and the edges are
based on the variance of the branch lengths of the ZLW trees built from them.

    Copyright (C) 2020 Norbert Bátfai, batfai.norbert@inf.unideb.hu, nbatfai@gmail.com

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

 Usage

batfai@bhaxor:~/Dokumentumok/BHS/z3a7-z3a19/ZLWSimilarity/COVID19$ g++-8 main.cpp -o main -std=c++17 -lstdc++fs -lboost_system

batfai@bhaxor:~/Dokumentumok/BHS/z3a7-z3a19/ZLWSimilarity/COVID19$ ./main
genomes/MT019529China:Wuhan2019-12-23  7.07744,  8.24597,  4.68211,  8.33276,  4.64213,  5.03486,  5.08847,  8.11385,  9.14629,  8.25817,  5.07464,  5.18237,  5.07705,  8.21414,  7.64712,  5.30149,  8.03237, 
genomes/MN988713USA:IL2020-01-21  8.57755,  7.51304,  4.74839,  8.32449,  4.76347,  5.08225,  5.15617,  7.97471,  9.17618,   7.9953,  5.05921,  5.24564,  5.12254,  8.00464,   8.3898,  5.16492,  8.23553, 
genomes/AF0868332EbolaMayingaZaire1976  4.70202,  4.61094,  6.18036,   4.6063,  5.28889,  4.73818,  4.66391,   4.6061,  4.69708,  4.69166,  4.64563,   4.7193,  4.65264,  4.63523,  4.64305,  4.65322,  4.66334, 
genomes/MT276326USA:GA2020-02-29  8.31965,  8.48266,  4.68007,  7.10811,  4.71693,  5.03778,   5.0611,  7.86583,  9.07221,  8.12748,  5.04882,  5.17649,  5.07616,  7.96762,  8.43238,  5.24068,  7.96577, 
genomes/KY7860271Ebola2001  4.61138,  4.61262,  5.28136,  4.66279,  6.17925,  4.65876,  4.66353,  4.66276,  4.71228,  4.61836,  4.59617,  4.66887,  4.63511,  4.61392,  4.61731,  4.65339,  4.63884, 
genomes/MT258383USA:CA2020-03-18  5.00394,  5.11712,  4.66524,  5.03896,  4.70919,  7.51941,  5.08022,  5.07321,   5.0987,  5.05594,  6.02664,  5.21926,  5.13298,  5.00896,  5.09807,  5.14671,   5.0731, 
genomes/LR757998China:Wuhan2019-12-26  5.01389,  5.01206,  4.67864,  5.07091,  4.70726,  5.10447,  7.04955,  5.05048,  5.12138,  5.01247,    4.957,  5.04339,  5.13796,  5.03808,  5.03483,  5.12902,  5.06494, 
genomes/MT027062USA:CA2020-01-29  8.06519,  7.82612,  4.72263,  8.08612,  4.71372,   5.0766,  5.10415,  7.01707,  9.02947,  7.75444,  5.02572,  5.16736,  5.13042,  7.81059,  7.91998,  5.18167,  7.82781, 
genomes/MT263459USA:WA2020-03-24  9.07891,  9.23275,  4.74829,  9.31324,   4.6962,  5.15247,  5.12293,  9.18074,  7.34252,  9.13287,  5.07186,  5.22282,   5.1426,  9.21415,  9.04331,  5.29182,  9.28602, 
genomes/MT044257USA:IL2020-01-28   8.4215,  7.68525,  4.63003,  8.19231,  4.65335,  5.09452,  5.07436,  7.98678,  9.07301,  7.07453,  5.03064,  5.17128,  5.07868,   7.7997,   8.0502,  5.20309,  8.16779, 
genomes/MT259226China:Hubei,Wuhan2020-01-10  5.06455,   5.0094,  4.70532,  5.04304,  4.64111,  6.27216,  5.03138,  5.01942,    5.111,  5.03023,  6.99871,  5.14022,  5.14702,   5.0492,   5.0966,  5.23481,  5.07534, 
genomes/MN938384China:Shenzhen2020-01-10  5.17222,  5.19948,  4.70795,  5.21511,  4.66317,  5.16364,  5.05079,  5.19428,  5.25967,  5.18273,  5.14971,  7.13492,  5.04069,  5.11741,  5.22939,  7.08738,  5.18268, 
genomes/MT039873China:Hangzhou2020-01-20  5.15764,  5.14071,  4.67502,  5.16197,  4.65603,  5.20486,   5.1138,  5.20562,  5.17914,  5.13724,  5.10105,  5.02323,  7.11266,   5.1967,  5.11056,  5.07628,   5.1118, 
genomes/MN985325USA:WA2020-01-19  8.22962,  7.91536,  4.71701,  7.87661,  4.69074,  5.02946,  5.04098,  7.98663,  9.09441,  7.75981,  5.03991,   5.1939,  5.12385,  7.05334,  7.92257,   5.2454,    7.771, 
genomes/MN908947China2019-12  7.78175,  8.11933,   4.6897,  8.37498,  4.68504,  5.09976,  5.13593,  7.88918,  8.70295,  8.20908,  5.04444,  5.10502,  5.11087,  7.83087,  7.06597,  5.23824,  7.92715, 
genomes/MN996527China:Wuhan2019-12-30  5.17688,  5.20121,  4.71535,  5.21069,  4.66698,  5.21895,  5.15342,  5.21731,  5.24291,  5.23004,  5.14305,   6.4373,  5.07766,  5.24572,   5.2564,   7.1135,  5.25721, 
genomes/MN994468USA:CA2020-01-22   8.2901,  8.17468,  4.67215,  7.85999,  4.70498,   5.0669,  5.11159,  7.97114,  9.01741,  7.90129,  5.04901,  5.20514,  5.09264,  7.60894,  7.87784,  5.23834,  7.15475, 


4.64213 0 0 1 0 1 1 1 0 0 0 1 1 1 0 0 1 0 
4.74839 0 0 1 0 1 1 1 0 0 0 1 1 1 0 0 1 0 
4.6061 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
4.68007 0 0 1 0 1 1 1 0 0 0 1 1 1 0 0 1 0 
4.59617 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 1 1 
4.66524 1 1 1 1 1 0 1 1 1 1 0 1 1 1 1 1 1 
4.67864 1 1 1 1 1 1 0 1 1 1 1 1 1 1 1 1 1 
4.71372 0 0 1 0 1 1 1 0 0 0 1 1 1 0 0 1 0 
4.6962 0 0 1 0 1 1 1 0 0 0 1 1 1 0 0 1 0 
4.63003 0 0 1 0 1 1 1 0 0 0 1 1 1 0 0 1 0 
4.64111 1 1 1 1 1 0 1 1 1 1 0 1 1 1 1 1 1 
4.66317 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 0 1 
4.65603 1 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 1 
4.69074 0 0 1 0 1 1 1 0 0 0 1 1 1 0 0 1 0 
4.68504 0 0 1 0 1 1 1 0 0 0 1 1 1 0 0 1 0 
4.66698 1 1 1 1 1 1 1 1 1 1 1 0 1 1 1 0 1 
4.67215 0 0 1 0 1 1 1 0 0 0 1 1 1 0 0 1 0 

batfai@bhaxor:~/Dokumentumok/BHS/z3a7-z3a19/ZLWSimilarity/COVID19$ dot -Tpng teszt.dot -o teszt.png

batfai@bhaxor:~/Dokumentumok/BHS/z3a7-z3a19/ZLWSimilarity/COVID19$ eog teszt.png 
 
        
 Version history

    BHAX 391 adás - Lab4.: Matrix design init hack, COVID19 - Corona Em. Brdc. - Stay Home
    https://www.twitch.tv/videos/588138046
    https://youtu.be/oLErhmTA2Z0

    BHAX 407 adás - Lab5.: Matrix design/2 init hack, COVID19 - Corona Em. Brdc. - Stay Home
    lásd még Stroustrup könyv, Bjarne Stroustrup: A C++ Programozási nyelv I-II. Kiskapu, 2001, II. kötet, 906. o.
    https://www.twitch.tv/videos/593713517
    https://youtu.be/dxsYdX0RoDM

    BHAX 414 adás - Lab6.: Matrix design/3 init hack, COVID19 - Corona Em. Brdc. - Stay Home
    https://www.twitch.tv/videos/595495585

    BHAX 438. adás - Lab.: Matrix design/4 init hack, COVID19 - Cov. Em. Brdc. - Stay Home
    https://www.twitch.tv/videos/602327773

*/

#include "zlwsim.h"

#include <fstream>
#include <iomanip>
#include <filesystem>
#include <valarray>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>

template <typename NumT>
class Slice_iter {
private:
     std::valarray<NumT> *vap;
     std::slice sl;
public:
     Slice_iter ( std::valarray<NumT> *vap, std::slice sl ) :vap ( vap ), sl ( sl ) {}


     NumT & operator[] ( int n )
     {
          return ( *vap ) [sl.start() + n*sl.stride()];
     }

     NumT min()
     {
          std::valarray<NumT> row = ( *vap ) [sl];
          return row.min();
     }

     NumT max()
     {
          std::valarray<NumT> row = ( *vap ) [sl];
          return row.max();
     }
     
     double mean()
     {
          std::valarray<NumT> row = ( *vap ) [sl];
          return
               static_cast<double> ( row.sum() )
               /
               static_cast<double> ( row.size() );
     }

};

template <typename NumT>
class Matrix {
private:
     std::valarray<NumT> *vap;
     int nor, noc;
public:
     Matrix ( int nor, int noc ) : nor ( nor ), noc ( noc ), vap ( new std::valarray<NumT> ( nor*noc ) ) {}
     ~Matrix()
     {
          delete vap;
     }

     Slice_iter<NumT> row ( int n )
     {
          return Slice_iter<NumT> ( vap, std::slice ( n*noc, noc, 1 ) );
     }

     Slice_iter<NumT> operator[] ( int n )
     {
          return row ( n );
     }

     double mean()
     {

          return
               static_cast<double> ( ( *vap ).sum() )
               /
               static_cast<double> ( ( *vap ).size() );
     }

};

typedef boost::adjacency_list < boost::listS, boost::vecS, boost::directedS,
        boost::property < boost::vertex_name_t, std::string,
        boost::property<boost::vertex_index2_t, int >> ,
        boost::property<boost::edge_weight_t, int >> ZLWGraph;

typedef boost::graph_traits<ZLWGraph>::vertex_descriptor ZLWGraphVertex;
typedef boost::property_map<ZLWGraph, boost::vertex_name_t>::type VertexNameMap;
typedef boost::property_map<ZLWGraph, boost::vertex_index2_t>::type VertexIndexMap;

void print_graph ( ZLWGraph &graph, VertexNameMap &v2str, std::string &fname )
{

     std::fstream graph_log ( fname + ".dot" , std::ios_base::out );
     boost::write_graphviz ( graph_log, graph, boost::make_label_writer ( v2str ) );

}

int main()
{

     std::string path {"genomes"};

     auto di = std::filesystem::directory_iterator ( path );
     int size = std::distance ( std::filesystem::begin ( di ), std::filesystem::end ( di ) );

     //std::cout << size << std::endl;

     Matrix<double> m {size, size};

     int r {0}, c {0};

     for ( const auto & entry1: std::filesystem::directory_iterator ( path ) ) {

          std::cout << entry1.path().string() << " ";

          for ( const auto & entry2: std::filesystem::directory_iterator ( path ) ) {

               ZLWTree<char, '/', '0'> zlwBt;

               std::ifstream ifs1 {entry1.path().string() };
               std::istreambuf_iterator<char> isbi1End;

               std::ifstream ifs2 {entry2.path().string() };
               std::istreambuf_iterator<char> isbi2End;

               for ( std::istreambuf_iterator<char> isbi1 {ifs1}, isbi2 {ifs2};
                         isbi1 != isbi1End || isbi2 != isbi2End;
                   ) {

                    if ( isbi1 != isbi1End ) {
                         zlwBt << *isbi1;
                         ++isbi1;
                    }
                    if ( isbi2 != isbi2End ) {
                         zlwBt << *isbi2;
                         ++isbi2;
                    }

               }

               zlwBt.eval();
               std::cout << std::setw ( 8 ) << ( m[r][c] = zlwBt.getVar() ) << ", ";
               ++c;
          }

          ++r;
          c=0;
          std::cout << std::endl;
     }

     std::cout << std::endl;
     std::cout << std::endl;

     double mean = m.mean();

     Matrix<int> gm {size, size};

     for ( int i {0}; i<size; ++i ) {

          std::cout << m[i].min() << " ";   
         
          for ( int j {0}; j<size; ++j ) {
               if ( m[i][j] < mean )
                    gm[i][j] = 1;
               else
                    gm[i][j] = 0;

               std::cout << gm[i][j] << " ";
          }

          std::cout << std::endl;
     }

     ZLWGraph zlw_graph;
     VertexNameMap v2str = boost::get ( boost::vertex_name, zlw_graph );
     VertexIndexMap v2idx = boost::get ( boost::vertex_index2, zlw_graph );
     std::map <std::string, ZLWGraphVertex> str2v;

     int idx {0};
     for ( const auto & entry1: std::filesystem::directory_iterator ( path ) ) {

          ZLWGraphVertex v = boost::add_vertex ( zlw_graph );
          v2str[v] = entry1.path().string();
          v2idx[v] = idx++;
          str2v[entry1.path().string()] = v;
     }

     r = c = 0;
     for ( const auto & entry1: std::filesystem::directory_iterator ( path ) ) {

          for ( const auto & entry2: std::filesystem::directory_iterator ( path ) ) {

               if ( m[r][c] > m[r].mean() )
                    boost::add_edge ( str2v[entry1.path().string()], str2v[entry2.path().string()], 1, zlw_graph );

               ++c;
          }
          ++r;
          c=0;

     }

     std::string fname {"teszt"};
     print_graph ( zlw_graph, v2str, fname );

}
